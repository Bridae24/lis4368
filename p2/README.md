> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Web Applications Development

## Bridae White

### Assignment #5 Requirements:
1. Add files (modify.jsp & customer.jsp)
2. MVC framework
3. Compile Class Files
4. Provide CRUD functionality 




#### README.md file should include the following items:
* Screenshots


#### Assignment Screenshots:

Screenshot of Valid User Form Entry

![screenshot of valid user form entry](img/valid_uf.png)

Screenshot of Thanks page:

![screenshot of thanks page](img/thanks.png)

Screenshot of Display Data:

![screenshot of display data](img/dd.png)

Screenshot of Valid User Form Entry 2

![screenshot of user entry form 2](img/valid_uf2.png)

Screenshot of Display Modified Data:

![screenshot of display modified data](img/modify.png)

Screenshot of Delete Data:

![screenshot of delete data](img/delete.png)