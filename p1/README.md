> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Web Applications Development

## Bridae White

### Project 1 Requirements:
1. Watch Tutorial Videos
2. Create my online portfolio
3. Modify index.jsp and review code
4. Add jQuery validation
5. Add entity attributes


#### README.md file should include the following items:
* Sreenshot of LIS4368 Portal (Main/Splash Page)
* Screenshot of faild validation
* Screenshot of passed validation


#### Assignment Screenshots:

*Screenshot of ERD*:
![screenshot of portal main page](img/portal_main.png)
![screenshot of failed validation](img/fail_valid.png)
![screenshot of passed validation](img/pass_valid.png)
