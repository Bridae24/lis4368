-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema bw14k
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `bw14k` ;

-- -----------------------------------------------------
-- Schema bw14k
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bw14k` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `bw14k` ;

-- -----------------------------------------------------
-- Table `bw14k`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bw14k`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bw14k`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) NOT NULL,
  `pst_phone` BIGINT NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bw14k`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bw14k`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bw14k`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT NOT NULL,
  `cus_phone` BIGINT NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bw14k`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bw14k`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bw14k`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `bw14k`.`petstore` (`pst_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `bw14k`.`customer` (`cus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `bw14k`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `bw14k`;
INSERT INTO `bw14k`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd`, `pst_notes`) VALUES (DEFAULT, 'Petco', '1080 Stuart St', 'Tallahassee', 'FL', 32308, 8503631789, 'pco@gmail.com', 'www.petco.com', 3773882, NULL);
INSERT INTO `bw14k`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd`, `pst_notes`) VALUES (DEFAULT, 'Panhandle Pet Supply', '3546 Naples St', 'Miami', 'FL', 43298, 3054847583, 'pps@yahoo.com', 'www.panpetsupply.com', 48499483, NULL);
INSERT INTO `bw14k`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd`, `pst_notes`) VALUES (DEFAULT, 'Carol\'s Critters', '983 Cheryl Ln', 'Jacksonville', 'FL', 56372, 9044950494, 'ccrits@yahoo.com', 'www.ccritters.com', 783874894, NULL);
INSERT INTO `bw14k`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd`, `pst_notes`) VALUES (DEFAULT, 'Naturally Healthy Pets', '1234 Chapel St', 'Panama City Beach', 'FL', 62534, 8508796450, 'nathpets@yahoo.com', 'www.nathealthpets.com', 26638826.36, NULL);
INSERT INTO `bw14k`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd`, `pst_notes`) VALUES (DEFAULT, 'Paws', '8729 Douglas Dr', 'Pensacola', 'FL', 72636, 4347899733, 'paws17@gmail.com', 'www.paws.com', 63748837.25, NULL);
INSERT INTO `bw14k`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd`, `pst_notes`) VALUES (DEFAULT, 'Petsmart', '5546 Herald Plaza', 'Atlanta', 'GA', 28499, 9548858959, 'psmart@gmail.com', 'www.petsmart.com', 6278383.37, NULL);
INSERT INTO `bw14k`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd`, `pst_notes`) VALUES (DEFAULT, 'Pet Mania', '808 Heartbreak Ln', 'Charlotte', 'NC', 84736, 5257342355, 'pmania@gmail.com', 'www.petmania.com', 62788383.33, NULL);
INSERT INTO `bw14k`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd`, `pst_notes`) VALUES (DEFAULT, 'Pet Warehouse', '1002 Lake Dr', 'Charleston', 'SC', 63749, 6477566475, 'pwhouse@gmail.com', 'www.petwhouse.com', 367388, NULL);
INSERT INTO `bw14k`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd`, `pst_notes`) VALUES (DEFAULT, 'Pet Stop', '9845 N Shore Dr', 'Miami', 'FL', 43648, 3058894641, 'petstop@aol.com', 'www.petstop.com', 8993922, NULL);
INSERT INTO `bw14k`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd`, `pst_notes`) VALUES (DEFAULT, 'HuggaMugga\'s', '2543 Modisette Rd', 'Miami', 'FL', 47388, 3056782992, 'hugmug@yahoo.com', 'www.huggamugga.com', 7784938, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bw14k`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `bw14k`;
INSERT INTO `bw14k`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Bridgette', 'Grandberry', '1872 Mary St', DEFAULT, 'FL', 73748, 3057482635, 'bg14@gmail.com', 3884.13, 848, NULL);
INSERT INTO `bw14k`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'David', 'Witherson', '3940 Gaudens Rd', DEFAULT, 'FL', 53488, 4048277373, 'bigdav@yahoo.com', 6477, 667, NULL);
INSERT INTO `bw14k`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Kimberly', 'Stanford', '374 Stewart Ave', DEFAULT, 'FL', 84036, 9047378383, 'kimbo@aol.com', 746.32, 734, NULL);
INSERT INTO `bw14k`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Craig', 'Peterson', '4785 Canal St', DEFAULT, 'FL', 52739, 8505536647, 'craigp@yahoo.com', 637.48, 523, NULL);
INSERT INTO `bw14k`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Joshua', 'Wilkinson', '758 Terrace Dr', DEFAULT, 'FL', 95867, 8502553748, 'joshposh@gmail.com', 5663, 653, NULL);
INSERT INTO `bw14k`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Charles', 'Williams', '2637 Kendale Lakes', DEFAULT, 'FL', 17274, 3057783834, 'cjt23@yahoo.com', 2838, 377, NULL);
INSERT INTO `bw14k`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Tony', 'Rogers', '3648 Talbot Rd', DEFAULT, 'FL', 73648, 7839999993, 'tonytone@gmail.com', 784, 627, NULL);
INSERT INTO `bw14k`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Trenton', 'Cambell', '7484 Van Buren St', DEFAULT, 'FL', 62384, 7366483633, 'trtbrb@gmail.com', 637.54, 794.54, NULL);
INSERT INTO `bw14k`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Terry', 'Richards', '9475 Venetia Terrace', DEFAULT, 'FL', 62784, 5627394773, 'terrycherry@yahoo.com', 762.83, 684, NULL);
INSERT INTO `bw14k`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Shelia', 'Thomas', '6739 Virginia St', DEFAULT, 'FL', 63748, 6278476635, 'ohshelia34@aol.com', 892, 243.50, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bw14k`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `bw14k`;
INSERT INTO `bw14k`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 1, 'Labrador Retriever', 'f', 250, 290, 4, 'tan', '2014-05-03', 'y', 'n', NULL);
INSERT INTO `bw14k`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 2, 'Siberian Husky', 'm', 175, 300, 2, 'brown/white', '2016-07-21', 'y', 'y', NULL);
INSERT INTO `bw14k`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 3, 'Beagle', 'm', 195, 275, 4, 'brown', '2015-08-24', 'y', 'y', NULL);
INSERT INTO `bw14k`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 4, 'Cockatiel', 'm', 255, 335, 5, 'green', NULL, 'n', 'n', NULL);
INSERT INTO `bw14k`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 5, 'Lorikeet', 'f', 265, 300, 6, 'orange', NULL, 'n', 'n', NULL);
INSERT INTO `bw14k`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 6, 6, 'Parrot', 'f', 200, 250, 5, 'yellow/green', '2013-05-09', 'n', 'n', NULL);
INSERT INTO `bw14k`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 7, 7, 'Boa Constrictor', 'f', 180, 260, 10, 'brown', '2012-01-31', 'n', 'n', NULL);
INSERT INTO `bw14k`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 8, 8, 'Ball Python', 'm', 210, 300, 12, 'black', NULL, 'n', '2006-09-08', NULL);
INSERT INTO `bw14k`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 9, 9, 'Hamster', 'm', 50, 125, 3, 'brown/white', NULL, 'y', 'y', NULL);
INSERT INTO `bw14k`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 10, 10, 'Gerbil', 'f', 75, 150, 2, 'black/white', '2010-06-10', 'y', 'n', NULL);

COMMIT;

