> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Web Applications Development

## Bridae White

### Assignment #3 Requirements:
1. Watch Tutorial Videos
2. Create MySQL ERD
3. Forward Engineer ERD
4. Create MySQL Script
5. Debug servlet


#### README.md file should include the following items:
* Assessment links 
* Screenshot of ERD


#### Assignment Screenshots:

*Screenshot of ERD*:

![screenshot of ERD](img/a3.png)

*Assessment Links:*

[a3.mwb](docs/a3.mwb "a3.mwb")

[a3.sql](docs/a3.sql "a3.sql")