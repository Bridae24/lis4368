> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 Advanced Web Applications Development

## Bridae White

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Git
    - Install Tomcat
    - Modify files
    - Setup SSH
    - Push files to bitbucket

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MYSQL
    - Develop and Deploy a WebApp
    - Deploy a servlet
    - Debug servlet

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Watch Videos
	- Create MySQL ERD
	- Create MySQL script

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Watch Videos
    - Create Online Portfolio
    - Modify Script
    - Add jQuery and entity attributes

5.  [A4 README.md](a4/README.md "My A4 README.md file")
    - Watch tutorial videos
    - Modify code
    - Passed/Failed validation

6.  [A5 README.md](a5/README.md "My A5 README.md file")
    - Watch tutorial videos
    - Server-side validation
    - Database data entries

7.  [P2 README.md](p2/README.md "My P2 README.md file")
    - MVC framework
    - Provide CRUD functionality
    - Add files
    - Server-side validation