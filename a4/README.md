> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Web Applications Development

## Bridae White

### Assignment #4 Requirements:
1. Watch Tutorial Videos
2. Modify Code
2. Compile Class Files
3. Failed Validation 
4. Passed Validation



#### README.md file should include the following items:
* Assessment links 
* Screenshots of failed/passed validation


#### Assignment Screenshots:

Screenshot of Failed Validation*:

![screenshot of failed validation](img/fail.png)

Screenshot of Passed Screenshots

![screenshot of passed validation](img/pass.png)