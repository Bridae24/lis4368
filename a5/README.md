> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Web Applications Development

## Bridae White

### Assignment #5 Requirements:
1. Watch Tutorial Videos
2. Modify Code
2. Compile Class Files
3. Failed Validation 
4. Passed Validation
5. Database Data Entries



#### README.md file should include the following items:
* Screenshots


#### Assignment Screenshots:

Screenshot of Valid User Form Entry

![screenshot of valid user form entry](img/usrform.png)

Screenshot of Passed Validation*:

![screenshot of passed validation](img/pass.png)

Screenshot of Database Entry 

![screenshot of database entry](img/dbentry.png)