> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Bridae White

### Assignment 1 Requirements:

*Three Parts:

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1 - 4)


#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above);
* Screenshot of running http://localhot:9999 (#2 above, Step #4(b) in tutorial);
* git commands w/short descriptions;
*Bitbucket repo links: a) this assignment and b) the completed tutorial above (bitbucketstationlocations).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. Git init- Create an empty Git repository or reinitialize an existing one
2. Git status-  Show the working tree status
3. Git add- Add file contents to the index
4. Git commit- Record changes to the repository
5. Git push- Update remote refs along with associated objects
6. Git pull- Fetch from and integrate with another repository or a local branch
7. Git stash- Stash the changes in a dirty working directory away

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![tomcat.png](img/tomcat.png)

*Screenshot of running java Hello*:

![jdk_install.png](img/jdk_install.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Bridae24/bitbucketstationlocations "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/Bridae24/myteamquotes "My Team Quotes Tutorial")
