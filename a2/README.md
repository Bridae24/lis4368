> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Web Applications Development

## Bridae White

### Assignment #2 Requirements:
1. Install MYSQL
2. Develop and Deploy a WebApp
3. Deploy a servlet
4. Debug servlet

*Assessment Links:*

[Hello Link](http://localhost:9999/hello "Hello")

[HelloHome](http://localhost:9999/hello/HelloHome.html "HelloHome")

[sayhello](http://localhost:9999/hello/sayhello "sayhello")

[querybook](http://localhost:9999/hello/querybook.html "querybook")

[sayhi](http://localhost:9999/hello/sayhi "sayhi")


#### README.md file should include the following items:
* Assessment links 
* Screenshot of query results


#### Assignment Screenshots:

*Screenshot of the query results*:

![query results screenshot](img/queryresults.png)
